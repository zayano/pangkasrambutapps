package com.example.zayano.test;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


public class DataHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "pangkasrambut.db";
    private static final int DATABESE_VERSION = 1;
    public DataHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABESE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        String sql = "create table penjasa(no integer primary key, nama text null, kawasan text null, no_hp integer null, harga_dewasa text null, harga_anak text null);";
        Log.d("Data", "onCreate: " + sql);
        db.execSQL(sql);
        sql = "INSERT INTO penjasa (no, nama, kawasan, no_hp, harga_dewasa, harga_anak) VALUES ('1', 'Pangkas Burhan', 'Margonda(Depok)', '6281316620436','Rp 15000', 'Rp 10000');";
        db.execSQL(sql);
        sql = "INSERT INTO penjasa (no, nama, kawasan, no_hp, harga_dewasa, harga_anak) VALUES ('2', 'Pangkas Rambut Fajar', 'Kalimulya(Depok)', '6287784297347','Rp 17000', 'Rp 11000');";
        db.execSQL(sql);
        sql = "INSERT INTO penjasa (no, nama, kawasan, no_hp, harga_dewasa, harga_anak) VALUES ('3', 'Pangkas Zaenudin', 'Cibinong(Bogor)', '628990667275','Rp 16000', 'Rp 10000');";
        db.execSQL(sql);
        sql = "INSERT INTO penjasa (no, nama, kawasan, no_hp, harga_dewasa, harga_anak) VALUES ('4', 'Pangkas Rafi', 'Bojonggede(Bogor)', '628564251426','Rp 18000', 'Rp 12000');";
        db.execSQL(sql);
        sql = "INSERT INTO penjasa (no, nama, kawasan, no_hp, harga_dewasa, harga_anak) VALUES ('5', 'Pangkas Asgar', 'Margonda(Depok)', '6281284226838','Rp 17000', 'Rp 13000');";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


}